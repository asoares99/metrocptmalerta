package br.com.mobilesolution.metroalerta.presenter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import java.util.ArrayList;
import java.util.List;
import br.com.mobilesolution.metroalerta.R;
import br.com.mobilesolution.metroalerta.api.ApiService;
import br.com.mobilesolution.metroalerta.interfaceUI.IMainPresenter;
import br.com.mobilesolution.metroalerta.modelDTO.ApiResponseGenericDTO;
import br.com.mobilesolution.metroalerta.modelDTO.DadosStatus;
import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;
import br.com.mobilesolution.metroalerta.modelDTO.StatusMetroCptmDTO;
import br.com.mobilesolution.metroalerta.ui.MainActivity;
import br.com.mobilesolution.metroalerta.util.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by alex.soares on 22/01/2018.
 */

public class MainPresenter implements IMainPresenter {
    private static Context context;

    @Override
    public void consultStatusMetroCPTM(final Context context) {
        this.context = context;
        if (Utils.checkInternetConnection(context)) {
            ApiService.iMetroCptmAPI service = ApiService.getApi();
            Call<ApiResponseGenericDTO<StatusMetroCptmDTO>> call = service.getStatusMetroCptm("getStatus");
            call.enqueue(new Callback<ApiResponseGenericDTO<StatusMetroCptmDTO>>() {
                @Override
                public void onResponse(Response<ApiResponseGenericDTO<StatusMetroCptmDTO>> response, Retrofit retrofit) {
                    final ApiResponseGenericDTO<StatusMetroCptmDTO> responseDTO = response.body();
                    if (response.code() >= 200 && response.code() < 300) {
                        arrayListStatusMetroCptm(responseDTO.getDados());
                    } else {
                        ((MainActivity) context).returnStatusMetroCptm(null);
                        showAlertErro(context.getString(R.string.message_alert_erro_api));
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ((MainActivity) context).returnStatusMetroCptm(null);
                    showAlertErro(context.getString(R.string.message_alert_erro_api));
                    System.out.println(t);
                }
            });

        } else {
            ((MainActivity) context).returnStatusMetroCptm(null);
            showAlertErro(context.getString(R.string.message_alert_erro_internet));
        }

    }

    @Override
    public void setFavoritesDefault(FavoritesDTO favoritesDTO) {
        if (Utils.checkInternetConnection(context)) {
            ApiService.iMetroCptmAPI service = ApiService.getApi();
            Call<ApiResponseGenericDTO<Object>> call = service.getFavorites(
                    "setFavorites", favoritesDTO.getApp(), favoritesDTO.getLinha_1(),
                    favoritesDTO.getLinha_2(), favoritesDTO.getLinha_3(), favoritesDTO.getLinha_4(),
                    favoritesDTO.getLinha_5(), favoritesDTO.getLinha_6(), favoritesDTO.getLinha_7(),
                    favoritesDTO.getLinha_8(), favoritesDTO.getLinha_9(), favoritesDTO.getLinha_10(),
                    favoritesDTO.getLinha_11(), favoritesDTO.getLinha_12(), favoritesDTO.getSeg(),
                    favoritesDTO.getTer(), favoritesDTO.getQua(), favoritesDTO.getQui(), favoritesDTO.getSex(),
                    favoritesDTO.getSab(), favoritesDTO.getDom(), favoritesDTO.getOnesignal_id());

            call.enqueue(new Callback<ApiResponseGenericDTO<Object>>() {
                @Override
                public void onResponse(Response<ApiResponseGenericDTO<Object>> response, Retrofit retrofit) {
                    final ApiResponseGenericDTO<Object> responseDTO = response.body();
                    if (response.code() >= 200 && response.code() < 300) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor mEdit1 = sp.edit();
                        mEdit1.putBoolean("favoritesDefault", false);
                        mEdit1.apply();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.getCause();
                }
            });
        }
    }



    private void showAlertErro(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Atenção")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        Dialog dialog = alert.create();
        dialog.show();
    }

    private void arrayListStatusMetroCptm(StatusMetroCptmDTO statusMetroCptmDTO) {
        List<DadosStatus> listStatusMetroCptm = new ArrayList<>();

        for (DadosStatus dadosStatus : statusMetroCptmDTO.getStatusMetro()) {
            listStatusMetroCptm.add(dadosStatus);
        }

        for (DadosStatus dadosStatus : statusMetroCptmDTO.getStatusViaQuatro()) {
            listStatusMetroCptm.add(dadosStatus);
        }

        for (DadosStatus dadosStatus : statusMetroCptmDTO.getStatusCPTM()) {
            listStatusMetroCptm.add(dadosStatus);
        }
        ((MainActivity) context).returnStatusMetroCptm(listStatusMetroCptm);
    }
}

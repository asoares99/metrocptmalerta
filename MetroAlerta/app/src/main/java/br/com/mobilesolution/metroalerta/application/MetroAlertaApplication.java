package br.com.mobilesolution.metroalerta.application;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import br.com.mobilesolution.metroalerta.ui.MainActivity;

/**
 * Created by alex.soares on 30/10/2017.
 */

public class MetroAlertaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Logging set to help debug issues, remove before releasing your app.
        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.WARN);

        OneSignal.startInit(this)
                .autoPromptLocation(false) // default call promptLocation later
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }


    private class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
        }
    }


    private class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl

            String openURL = null;
            Object activityToLaunch = MainActivity.class;

            if (data != null) {
                openURL = data.optString("openURL", null);
            }

            if (launchUrl != null && !launchUrl.equalsIgnoreCase("")) {
                Uri uri = Uri.parse(launchUrl);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            } else {
                if (actionType == OSNotificationAction.ActionType.ActionTaken) {
                    if (result.action.actionID.equals("id1")) {
                        activityToLaunch = MainActivity.class;
                    }
                }
                // The following can be used to open an Activity of your choice.
                // Replace - getApplicationContext() - with any Android Context.
                // Intent intent = new Intent(getApplicationContext(), YourActivity.class);
                Intent intent = new Intent(getApplicationContext(), (Class<?>) activityToLaunch);
                // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("openURL", openURL);
                // startActivity(intent);
                startActivity(intent);

            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


}

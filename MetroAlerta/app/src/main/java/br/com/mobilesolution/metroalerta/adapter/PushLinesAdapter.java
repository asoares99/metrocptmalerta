package br.com.mobilesolution.metroalerta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import java.util.List;

import br.com.mobilesolution.metroalerta.R;
import br.com.mobilesolution.metroalerta.modelDTO.DadosStatus;
import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;

/**
 * Created by alex.soares on 22/01/2018.
 */

public class PushLinesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DadosStatus> listStatusMetroCptmDTO;
    private Context context;
    private FavoritesDTO favoritesDTO;
    private ClickListener mClickListener;

    public PushLinesAdapter(List<DadosStatus> listStatusMetroCptmDTO, FavoritesDTO favoritesDTO, Context context) {
        this.listStatusMetroCptmDTO = listStatusMetroCptmDTO;
        this.favoritesDTO = favoritesDTO;
        this.context = context;
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        public Switch swtLinha;

        public ItemViewHolder(View view) {
            super(view);
            swtLinha = (Switch) view.findViewById(R.id.swtLinePush);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.push_lines_adapter, parent, false);
        return new PushLinesAdapter.ItemViewHolder(headerView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        PushLinesAdapter.ItemViewHolder itemViewHolder = (PushLinesAdapter.ItemViewHolder) holder;
        itemViewHolder.swtLinha.setText(listStatusMetroCptmDTO.get(position).getNome());

        if(favoritesDTO != null){
            switch (position) {
                case 0:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_1().equalsIgnoreCase("1"));
                    break;

                case 1:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_2().equalsIgnoreCase("1"));
                    break;

                case 2:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_3().equalsIgnoreCase("1"));
                    break;

                case 3:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_4().equalsIgnoreCase("1"));
                    break;

                case 4:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_5().equalsIgnoreCase("1"));
                    break;

                case 5:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_7().equalsIgnoreCase("1"));
                    break;

                case 6:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_8().equalsIgnoreCase("1"));
                    break;

                case 7:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_9().equalsIgnoreCase("1"));
                    break;

                case 8:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_10().equalsIgnoreCase("1"));
                    break;

                case 9:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_11().equalsIgnoreCase("1"));
                    break;

                case 10:
                    itemViewHolder.swtLinha.setChecked(favoritesDTO.getLinha_12().equalsIgnoreCase("1"));
                    break;
            }
        }

        itemViewHolder.swtLinha.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mClickListener.onClick(position, b);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private List<DadosStatus> getItems() {
        return listStatusMetroCptmDTO;
    }

    private void fillItems() {
        // this.items = Utils.getItemsWithHeaders(modelsDTOs, items);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (listStatusMetroCptmDTO != null) {
            return listStatusMetroCptmDTO.size();
        } else {
            return 0;
        }
    }

    public void setStatusMetroCptm(final List<DadosStatus> listStatusMetroCptmDTO) {
        this.listStatusMetroCptmDTO = listStatusMetroCptmDTO;
        fillItems();
    }


    public interface ClickListener {
        void onClick(int position, boolean checked);
    }

    public void setmClickListener(final ClickListener clickListener) {
        this.mClickListener = clickListener;
    }
}

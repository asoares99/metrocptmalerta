package br.com.mobilesolution.metroalerta.api;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import br.com.mobilesolution.metroalerta.modelDTO.ApiResponseGenericDTO;
import br.com.mobilesolution.metroalerta.modelDTO.StatusMetroCptmDTO;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by alex.soares on 16/10/2017.
 */

public class ApiService {
    private static iMetroCptmAPI iMetroCptmAPI;
    private static String URL_BASE = "https://www.mobilesolutions.com.br";
	
    public static iMetroCptmAPI getApiUrlCurrent() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.setWriteTimeout(120, TimeUnit.SECONDS);
        okClient.setConnectTimeout(120, TimeUnit.SECONDS);
        okClient.setReadTimeout(120, TimeUnit.SECONDS);
        okClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.header("Content-Type", "application/json");
                requestBuilder.header("Accept", "application/json");
                requestBuilder.header("SD-Language", "pt_BR");
                requestBuilder.header("SD-Platform", "Android");
                requestBuilder.method(original.method(), original.body());
                return chain.proceed(requestBuilder.build());
            }
        });
        Retrofit client = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        iMetroCptmAPI = client.create(iMetroCptmAPI.class);
        return iMetroCptmAPI;
    }

    public static iMetroCptmAPI getApi() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.setWriteTimeout(120, TimeUnit.SECONDS);
        okClient.setConnectTimeout(120, TimeUnit.SECONDS);
        okClient.setReadTimeout(120, TimeUnit.SECONDS);
        okClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.header("Content-Type", "application/json");
                requestBuilder.header("Accept", "application/json");
                requestBuilder.header("SD-Language", "pt_BR");
                requestBuilder.header("SD-Platform", "Android");
                requestBuilder.method(original.method(), original.body());
                return chain.proceed(requestBuilder.build());
            }
        });
        Retrofit client = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        iMetroCptmAPI = client.create(iMetroCptmAPI.class);
        return iMetroCptmAPI;
    }

    public interface iMetroCptmAPI {
        @GET("/app/trilhostatus/")
        Call<ApiResponseGenericDTO<StatusMetroCptmDTO>> getStatusMetroCptm(@Query("m") String m);

        @GET("app/trilhostatus/")
        Call<ApiResponseGenericDTO<Object>> getFavorites(
                @Query("m") String m,
                @Query("app") String app,
                @Query("linha_1") String linha_1,
                @Query("linha_2") String linha_2,
                @Query("linha_3") String linha_3,
                @Query("linha_4") String linha_4,
                @Query("linha_5") String linha_5,
                @Query("linha_6") String linha_6,
                @Query("linha_7") String linha_7,
                @Query("linha_8") String linha_8,
                @Query("linha_9") String linha_9,
                @Query("linha_10") String linha_10,
                @Query("linha_11") String linha_11,
                @Query("linha_12") String linha_12,
                @Query("seg") String seg,
                @Query("ter") String ter,
                @Query("qua") String qua,
                @Query("qui") String qui,
                @Query("sex") String sex,
                @Query("sab") String sab,
                @Query("dom") String dom,
                @Query("onesignal_id") String onesignal_id);
    }
}

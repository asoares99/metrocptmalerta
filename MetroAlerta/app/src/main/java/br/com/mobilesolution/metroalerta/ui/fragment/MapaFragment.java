package br.com.mobilesolution.metroalerta.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;

import br.com.mobilesolution.metroalerta.R;

public class MapaFragment extends Fragment {
    private View view;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mapa, container, false);
        configView();
        return view;
    }

    private void configView(){
        try {
            PhotoView photoView = (PhotoView) view.findViewById(R.id.photo_view);
            photoView.setImageResource(R.drawable.mapa);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }

    }
}

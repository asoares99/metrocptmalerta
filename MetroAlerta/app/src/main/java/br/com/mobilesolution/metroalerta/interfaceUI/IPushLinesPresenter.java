package br.com.mobilesolution.metroalerta.interfaceUI;

import android.content.Context;

import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;

/**
 * Created by alex.soares on 22/01/2018.
 */

public interface IPushLinesPresenter {
    void setFavoritesDefault(final Context context, final FavoritesDTO favoritesDTO);
}

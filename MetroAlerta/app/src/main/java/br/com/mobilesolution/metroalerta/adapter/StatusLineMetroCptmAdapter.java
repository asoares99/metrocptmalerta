package br.com.mobilesolution.metroalerta.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.mobilesolution.metroalerta.BuildConfig;
import br.com.mobilesolution.metroalerta.R;
import br.com.mobilesolution.metroalerta.modelDTO.DadosStatus;
import br.com.mobilesolution.metroalerta.modelDTO.StatusMetroCptmDTO;

/**
 * Created by alex.soares on 22/01/2018.
 */

public class StatusLineMetroCptmAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DadosStatus> listStatusMetroCptmDTO;
    private Context context;
    private StatusLineMetroCptmAdapter.ClickListener mClickListener;

    public StatusLineMetroCptmAdapter(List<DadosStatus> listStatusMetroCptmDTO, Context context) {
        this.listStatusMetroCptmDTO = listStatusMetroCptmDTO;
        this.context = context;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNome, tvStatus, tvHorario, tvDescricao;
        public ImageView imgIconeLinha;
        public RelativeLayout llContainerLinha;

        public ItemViewHolder(View view) {
            super(view);
            tvNome = (TextView) view.findViewById(R.id.txtNomeLinha);
            tvStatus = (TextView) view.findViewById(R.id.txtStatusLinha);
            tvHorario = (TextView) view.findViewById(R.id.txtDataAtualizacao);
            tvDescricao = (TextView) view.findViewById(R.id.txtDescricao);
            imgIconeLinha = (ImageView) view.findViewById(R.id.imgIconeLinha);
            llContainerLinha = (RelativeLayout) view.findViewById(R.id.llContainerLinha);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_line_adapter, parent, false);
        return new StatusLineMetroCptmAdapter.ItemViewHolder(headerView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        StatusLineMetroCptmAdapter.ItemViewHolder itemViewHolder = (StatusLineMetroCptmAdapter.ItemViewHolder) holder;

        itemViewHolder.tvNome.setText(listStatusMetroCptmDTO.get(position).getNome());
        itemViewHolder.tvStatus.setText(listStatusMetroCptmDTO.get(position).getStatus());

        String horario = listStatusMetroCptmDTO.get(position).getDataGeracao().substring(11);
        itemViewHolder.tvHorario.setText("atualizado as " + horario.substring(0, 5));

        if (listStatusMetroCptmDTO.get(position).getDescricao() != null && !listStatusMetroCptmDTO.get(position).getDescricao().equalsIgnoreCase("")) {
            itemViewHolder.tvDescricao.setText(listStatusMetroCptmDTO.get(position).getDescricao());
            itemViewHolder.tvDescricao.setVisibility(View.VISIBLE);
        }


        //Icone da Lista
        switch (listStatusMetroCptmDTO.get(position).getTipo()) {
            case "M":
                itemViewHolder.imgIconeLinha.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_metro));
                break;
            case "4":
                itemViewHolder.imgIconeLinha.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_viaquatro));
                break;
            case "C":
                itemViewHolder.imgIconeLinha.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cptm));
                break;
        }

        if (mClickListener != null) {
            ((ItemViewHolder) holder).llContainerLinha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onClick(v, position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (listStatusMetroCptmDTO != null) {
            return listStatusMetroCptmDTO.size();
        } else {
            return 0;
        }
    }

    public List<DadosStatus> getItems() {
        return listStatusMetroCptmDTO;
    }

    private void fillItems() {
        // this.items = Utils.getItemsWithHeaders(modelsDTOs, items);
        notifyDataSetChanged();
    }

    public interface ClickListener {
        void onClick(View view, int position);
    }

    public void setmClickListener(final StatusLineMetroCptmAdapter.ClickListener clickListener) {
        this.mClickListener = clickListener;
    }

    public void setStatusMetroCptm(final List<DadosStatus> listStatusMetroCptmDTO) {
        this.listStatusMetroCptmDTO = listStatusMetroCptmDTO;
        fillItems();
    }

}

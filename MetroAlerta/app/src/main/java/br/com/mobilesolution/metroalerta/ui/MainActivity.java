package br.com.mobilesolution.metroalerta.ui;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.onesignal.OneSignal;
import br.com.mobilesolution.metroalerta.adapter.StatusLineMetroCptmAdapter;
import br.com.mobilesolution.metroalerta.interfaceUI.IMainPresenter;
import br.com.mobilesolution.metroalerta.modelDTO.DadosStatus;
import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;
import br.com.mobilesolution.metroalerta.presenter.MainPresenter;
import io.fabric.sdk.android.Fabric;
import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import br.com.mobilesolution.metroalerta.R;
import br.com.mobilesolution.metroalerta.ui.fragment.MapaFragment;
import br.com.mobilesolution.metroalerta.ui.fragment.StatusFragment;

public class MainActivity extends AppCompatActivity{
    private SectionsPagerAdapter adapterSectionsPager;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar = null;
    private List<String> listFragments = new Vector<String>();
    private AdView mAdViewSense;
    private ProgressDialog progressDialog;
    private IMainPresenter presenter;
    private List<DadosStatus> listStatusMetroCptm;
    private StatusLineMetroCptmAdapter statusLineMetroCptmAdapter;
    private FavoritesDTO favoritesDTO;
    private String oneSignalId = null;
    private SharedPreferences spSharedPreferences;
    private MenuItem menuItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenter();


        //Get IDUser
        spSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        getIDRegistrationOnSignal();

        //AdSense
        adSenseGoogle();

        // IMPLEMENTS TOOLBAR
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        adapterSectionsPager = new SectionsPagerAdapter(getSupportFragmentManager());

        callPresenterStatusMetroCptm();

        //Fabric Analytics
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(false)
                .build();
        Fabric.with(fabric);
    }


    public void callPresenterStatusMetroCptm() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(" Metrô Alerta ");
        progressDialog.setMessage("Carregando...");
        presenter.consultStatusMetroCPTM(this);
        progressDialog.show();
    }


    private void adSenseGoogle() {
        String id_adSenseGoogle = getString(R.string.id_adSenseMobileADS);
        MobileAds.initialize(getApplicationContext(), id_adSenseGoogle);
        mAdViewSense = (AdView) findViewById(R.id.adViewSenseGoogle);
        mAdViewSense.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewSense.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                adSenseGoogle();
                super.onAdFailedToLoad(i);
            }
        });
        mAdViewSense.loadAd(adRequest);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            listFragments.add(StatusFragment.class.getName());
            listFragments.add(MapaFragment.class.getName());
        }

        @Override
        public Fragment getItem(int position) {
            //return PlaceholderFragment.newInstance(position);
            return Fragment.instantiate(getBaseContext(), listFragments.get(position));
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "STATUS";
                case 1:
                    return "MAPA";
            }
            return null;
        }
    }

    public void returnStatusMetroCptm(List<DadosStatus> listStatusMetroCptm) {
        progressDialog.cancel();
        if (listStatusMetroCptm != null) {
            this.listStatusMetroCptm = listStatusMetroCptm;
            // Set up the ViewPager with the sections adapter.
            viewPager = (ViewPager) findViewById(R.id.container);
            viewPager.setAdapter(adapterSectionsPager);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
        }
        setFavoritesDefault();
    }

    private void setFavoritesDefault() {
        if (oneSignalId == null || oneSignalId.equalsIgnoreCase("")) {
            getIDRegistrationOnSignal();
        } else {
            favoritesDTO = new FavoritesDTO();
            favoritesDTO.setApp("metroalerta");
            favoritesDTO.setLinha_1("1");
            favoritesDTO.setLinha_2("1");
            favoritesDTO.setLinha_3("1");
            favoritesDTO.setLinha_4("1");
            favoritesDTO.setLinha_5("1");
            favoritesDTO.setLinha_6("1");
            favoritesDTO.setLinha_7("1");
            favoritesDTO.setLinha_8("1");
            favoritesDTO.setLinha_9("1");
            favoritesDTO.setLinha_10("1");
            favoritesDTO.setLinha_11("1");
            favoritesDTO.setLinha_12("1");
            favoritesDTO.setSeg("1");
            favoritesDTO.setTer("1");
            favoritesDTO.setQua("1");
            favoritesDTO.setQui("1");
            favoritesDTO.setSex("1");
            favoritesDTO.setSab("1");
            favoritesDTO.setDom("1");
            favoritesDTO.setOnesignal_id(oneSignalId);

            if (spSharedPreferences.getBoolean("favoritesDefault", true)) {
                presenter.setFavoritesDefault(favoritesDTO);
                SharedPreferences.Editor mEdit1 = spSharedPreferences.edit();
                Gson gson = new Gson();
                String json = gson.toJson(favoritesDTO);
                mEdit1.putString("favorites", json);
                mEdit1.commit();

                if (menuItem != null) {
                    menuItem.setVisible(true);
                }
            }
        }
    }


    public void getIDRegistrationOnSignal() {
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null) {
                    oneSignalId = userId;
                    setFavoritesDefault();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        this.menuItem = menu.findItem(R.id.action_settings);
        if (!spSharedPreferences.getBoolean("favoritesDefault", true)) {
            menu.findItem(R.id.action_settings).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent mIntent = new Intent(this, PushLinesActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("dadosLinhas", (Serializable) listStatusMetroCptm);
            mIntent.putExtras(mBundle);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivity(mIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
            } else {
                startActivity(mIntent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<DadosStatus> getStatusMetroCptm() {
        return listStatusMetroCptm;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            return super.onTouchEvent(event);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public StatusLineMetroCptmAdapter getAdapterModelAutoker() {
        statusLineMetroCptmAdapter = new StatusLineMetroCptmAdapter(listStatusMetroCptm, this);
        return statusLineMetroCptmAdapter;
    }
}

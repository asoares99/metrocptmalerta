package br.com.mobilesolution.metroalerta.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.google.gson.Gson;
import java.util.List;
import br.com.mobilesolution.metroalerta.R;
import br.com.mobilesolution.metroalerta.adapter.PushLinesAdapter;
import br.com.mobilesolution.metroalerta.interfaceUI.IPushLinesPresenter;
import br.com.mobilesolution.metroalerta.modelDTO.DadosStatus;
import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;
import br.com.mobilesolution.metroalerta.presenter.PushLinesPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PushLinesActivity extends AppCompatActivity {
    @BindView(R.id.rv_status_metro_cptm)
    RecyclerView rvStatusMetroCptm;

    private List<DadosStatus> listDadosLinhas;
    private FavoritesDTO favoritesDTO;
    private SharedPreferences spSharedPreferences;
    private IPushLinesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_lines);
        ButterKnife.bind(this, this);

        // IMPLEMENTS TOOLBAR
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        presenter = new PushLinesPresenter();

        Intent intent = getIntent();
        if (intent != null) {
            listDadosLinhas = (List<DadosStatus>) intent.getSerializableExtra("dadosLinhas");
        }

        spSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Gson gson = new Gson();
        String json = spSharedPreferences.getString("favorites", "");
        favoritesDTO = gson.fromJson(json, FavoritesDTO.class);
        initModelsAutomakerRecycleView();
    }


    private void initModelsAutomakerRecycleView() {
        PushLinesAdapter pushLinesAdapter = new PushLinesAdapter(listDadosLinhas, favoritesDTO, this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvStatusMetroCptm.setLayoutManager(mLayoutManager);
        pushLinesAdapter.setStatusMetroCptm(listDadosLinhas);
        pushLinesAdapter.notifyDataSetChanged();
        rvStatusMetroCptm.setAdapter(pushLinesAdapter);
        pushLinesAdapter.notifyDataSetChanged();
        pushLinesAdapter.setmClickListener(new PushLinesAdapter.ClickListener() {
            @Override
            public void onClick(int position, boolean checked) {
            setUpdateFavote(position, checked);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setUpdateFavote(int position, boolean check) {
        String status = (check) ? "1" : "0";
        switch (position) {
            case 0:
                favoritesDTO.setLinha_1(status);
                break;

            case 1:
                favoritesDTO.setLinha_2(status);
                break;

            case 2:
                favoritesDTO.setLinha_3(status);
                break;

            case 3:
                favoritesDTO.setLinha_4(status);
                break;

            case 4:
                favoritesDTO.setLinha_5(status);
                break;

            case 5:
                favoritesDTO.setLinha_7(status);
                break;

            case 6:
                favoritesDTO.setLinha_8(status);
                break;

            case 7:
                favoritesDTO.setLinha_9(status);
                break;

            case 8:
                favoritesDTO.setLinha_10(status);
                break;

            case 9:
                favoritesDTO.setLinha_11(status);
                break;

            case 10:
                favoritesDTO.setLinha_12(status);
                break;
        }

        SharedPreferences.Editor mEdit1 = spSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(favoritesDTO);
        mEdit1.putString("favorites", json);
        mEdit1.commit();
        presenter.setFavoritesDefault(this, favoritesDTO);
    }
}

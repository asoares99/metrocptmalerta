package br.com.mobilesolution.metroalerta.modelDTO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alex.soares on 16/10/2017.
 */

public class ApiResponseGenericDTO<T> {
    @SerializedName("result")
    private T Dados;

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    public T getDados() {
        return Dados;
    }

    public void setDados(T dados) {
        Dados = dados;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

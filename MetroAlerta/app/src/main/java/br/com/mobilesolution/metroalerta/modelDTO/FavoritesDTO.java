package br.com.mobilesolution.metroalerta.modelDTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by alex.soares on 23/01/2018.
 */

public class FavoritesDTO implements Serializable{
    String app;
    String linha_1;
    String linha_2;
    String linha_3;
    @SerializedName("linha_4")
    String linha_4;
    @SerializedName("linha_5")
    String linha_5;
    @SerializedName("linha_6")
    String linha_6;
    @SerializedName("linha_7")
    String linha_7;
    @SerializedName("linha_8")
    String linha_8;
    @SerializedName("linha_9")
    String linha_9;
    @SerializedName("linha_10")
    String linha_10;
    @SerializedName("linha_11")
    String linha_11;
    @SerializedName("linha_12")
    String linha_12;
    @SerializedName("seg")
    String seg;
    @SerializedName("ter")
    String ter;
    @SerializedName("qua")
    String qua;
    @SerializedName("qui")
    String qui;
    @SerializedName("sex")
    String sex;
    @SerializedName("sab")
    String sab;
    @SerializedName("dom")
    String dom;
    @SerializedName("onesignal_id")
    String onesignal_id;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getLinha_1() {
        return linha_1;
    }

    public void setLinha_1(String linha_1) {
        this.linha_1 = linha_1;
    }

    public String getLinha_2() {
        return linha_2;
    }

    public void setLinha_2(String linha_2) {
        this.linha_2 = linha_2;
    }

    public String getLinha_3() {
        return linha_3;
    }

    public void setLinha_3(String linha_3) {
        this.linha_3 = linha_3;
    }

    public String getLinha_4() {
        return linha_4;
    }

    public void setLinha_4(String linha_4) {
        this.linha_4 = linha_4;
    }

    public String getLinha_5() {
        return linha_5;
    }

    public void setLinha_5(String linha_5) {
        this.linha_5 = linha_5;
    }

    public String getLinha_6() {
        return linha_6;
    }

    public void setLinha_6(String linha_6) {
        this.linha_6 = linha_6;
    }

    public String getLinha_7() {
        return linha_7;
    }

    public void setLinha_7(String linha_7) {
        this.linha_7 = linha_7;
    }

    public String getLinha_8() {
        return linha_8;
    }

    public void setLinha_8(String linha_8) {
        this.linha_8 = linha_8;
    }

    public String getLinha_9() {
        return linha_9;
    }

    public void setLinha_9(String linha_9) {
        this.linha_9 = linha_9;
    }

    public String getLinha_10() {
        return linha_10;
    }

    public void setLinha_10(String linha_10) {
        this.linha_10 = linha_10;
    }

    public String getLinha_11() {
        return linha_11;
    }

    public void setLinha_11(String linha_11) {
        this.linha_11 = linha_11;
    }

    public String getLinha_12() {
        return linha_12;
    }

    public void setLinha_12(String linha_12) {
        this.linha_12 = linha_12;
    }

    public String getSeg() {
        return seg;
    }

    public void setSeg(String seg) {
        this.seg = seg;
    }

    public String getTer() {
        return ter;
    }

    public void setTer(String ter) {
        this.ter = ter;
    }

    public String getQua() {
        return qua;
    }

    public void setQua(String qua) {
        this.qua = qua;
    }

    public String getQui() {
        return qui;
    }

    public void setQui(String qui) {
        this.qui = qui;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSab() {
        return sab;
    }

    public void setSab(String sab) {
        this.sab = sab;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public String getOnesignal_id() {
        return onesignal_id;
    }

    public void setOnesignal_id(String onesignal_id) {
        this.onesignal_id = onesignal_id;
    }
}

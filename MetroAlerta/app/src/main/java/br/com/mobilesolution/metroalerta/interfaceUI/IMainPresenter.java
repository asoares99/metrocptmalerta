package br.com.mobilesolution.metroalerta.interfaceUI;

import android.content.Context;

import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;

/**
 * Created by alex.soares on 22/01/2018.
 */

public interface IMainPresenter {
    void consultStatusMetroCPTM(final Context context);
    void setFavoritesDefault(final FavoritesDTO favoritesDTO);
}

package br.com.mobilesolution.metroalerta.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.mobilesolution.metroalerta.R;
import br.com.mobilesolution.metroalerta.adapter.StatusLineMetroCptmAdapter;
import br.com.mobilesolution.metroalerta.ui.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusFragment extends Fragment {
    @BindView(R.id.status_metro_cptm)
    RecyclerView rvStatusMetroCptm;

    private StatusLineMetroCptmAdapter statusLineMetroCptmAdapter;
    private Context context;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_status, container, false);
        context = view.getContext();
        ButterKnife.bind(this, view);
        //configView();
        if(((MainActivity)context).getStatusMetroCptm()  != null ){
            statusLineMetroCptmAdapter = ((MainActivity)getActivity()).getAdapterModelAutoker();
            initModelsAutomakerRecycleView();
        }
        return view;
    }

    private void initModelsAutomakerRecycleView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvStatusMetroCptm.setLayoutManager(mLayoutManager);
        statusLineMetroCptmAdapter.setStatusMetroCptm(((MainActivity)getContext()).getStatusMetroCptm());
        statusLineMetroCptmAdapter.notifyDataSetChanged();
        rvStatusMetroCptm.setAdapter(statusLineMetroCptmAdapter);
        statusLineMetroCptmAdapter.notifyDataSetChanged();

    }



}

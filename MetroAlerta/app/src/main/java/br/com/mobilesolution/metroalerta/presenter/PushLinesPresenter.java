package br.com.mobilesolution.metroalerta.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import br.com.mobilesolution.metroalerta.api.ApiService;
import br.com.mobilesolution.metroalerta.interfaceUI.IPushLinesPresenter;
import br.com.mobilesolution.metroalerta.modelDTO.ApiResponseGenericDTO;
import br.com.mobilesolution.metroalerta.modelDTO.FavoritesDTO;
import br.com.mobilesolution.metroalerta.util.Utils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by alex.soares on 22/01/2018.
 */

public class PushLinesPresenter implements IPushLinesPresenter {
    private static Context context;
    @Override
    public void setFavoritesDefault(final Context context, FavoritesDTO favoritesDTO) {
        if (Utils.checkInternetConnection(context)) {
            ApiService.iMetroCptmAPI service = ApiService.getApi();
            Call<ApiResponseGenericDTO<Object>> call = service.getFavorites(
                    "setFavorites", favoritesDTO.getApp(), favoritesDTO.getLinha_1(),
                    favoritesDTO.getLinha_2(), favoritesDTO.getLinha_3(), favoritesDTO.getLinha_4(),
                    favoritesDTO.getLinha_5(), favoritesDTO.getLinha_6(), favoritesDTO.getLinha_7(),
                    favoritesDTO.getLinha_8(), favoritesDTO.getLinha_9(), favoritesDTO.getLinha_10(),
                    favoritesDTO.getLinha_11(), favoritesDTO.getLinha_12(), favoritesDTO.getSeg(),
                    favoritesDTO.getTer(), favoritesDTO.getQua(), favoritesDTO.getQui(), favoritesDTO.getSex(),
                    favoritesDTO.getSab(), favoritesDTO.getDom(), favoritesDTO.getOnesignal_id());

            call.enqueue(new Callback<ApiResponseGenericDTO<Object>>() {
                @Override
                public void onResponse(Response<ApiResponseGenericDTO<Object>> response, Retrofit retrofit) {
                    final ApiResponseGenericDTO<Object> responseDTO = response.body();
                    if (response.code() >= 200 && response.code() < 300) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor mEdit1 = sp.edit();
                        mEdit1.putBoolean("favoritesDefault", false);
                        mEdit1.apply();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.getCause();
                }
            });
        }
    }

}

package br.com.mobilesolution.metroalerta.modelDTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by alex.soares on 22/01/2018.
 */

public class StatusMetroCptmDTO  implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("Metro")
    List<DadosStatus> statusMetro;
    @SerializedName("ViaQuatro")
    List<DadosStatus> statusViaQuatro;
    @SerializedName("CPTM")
    List<DadosStatus> statusCPTM;

    public List<DadosStatus> getStatusMetro() {
        return statusMetro;
    }

    public List<DadosStatus> getStatusViaQuatro() {
        return statusViaQuatro;
    }

    public List<DadosStatus> getStatusCPTM() {
        return statusCPTM;
    }
}
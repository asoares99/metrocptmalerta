package br.com.mobilesolution.metroalerta.modelDTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by alex.soares on 22/01/2018.
 */

public class DadosStatus implements Serializable {
    @SerializedName("Status")
    String status;
    @SerializedName("Descricao")
    String descricao;
    @SerializedName("LinhaId")
    String linhaId;
    @SerializedName("Nome")
    String nome;
    @SerializedName("Tipo")
    String tipo;
    @SerializedName("DataGeracao")
    String dataGeracao;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLinhaId() {
        return linhaId;
    }

    public void setLinhaId(String linhaId) {
        this.linhaId = linhaId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDataGeracao() {
        return dataGeracao;
    }

    public void setDataGeracao(String dataGeracao) {
        this.dataGeracao = dataGeracao;
    }
}

package br.com.mobilesolution.metroalerta;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.mobilesolution.metroalerta.ui.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by alex.soares on 08/02/2018.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);


    @Test
    public void changeText_sameActivity() {
        // Type text and then press the button.
        try {
            Thread.sleep(4000);
            onView(withId(R.id.action_settings)).check(matches(isEnabled()));

            Thread.sleep(2000);
            onView(withId(R.id.action_settings)).perform(click());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
